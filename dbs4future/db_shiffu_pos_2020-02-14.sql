# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.37-MariaDB)
# Database: db_shiffu_pos
# Generation Time: 2020-02-14 16:31:58 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table shf_branch
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shf_branch`;

CREATE TABLE `shf_branch` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text,
  `telp` varchar(100) DEFAULT NULL,
  `hp` varchar(200) DEFAULT NULL,
  `logo` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table shf_kategori
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shf_kategori`;

CREATE TABLE `shf_kategori` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT NULL,
  `nama` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table shf_member_paket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shf_member_paket`;

CREATE TABLE `shf_member_paket` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT NULL,
  `paket_id` int(11) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table shf_produk
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shf_produk`;

CREATE TABLE `shf_produk` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `satuan_id` int(11) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table shf_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shf_roles`;

CREATE TABLE `shf_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `peran` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `shf_roles` WRITE;
/*!40000 ALTER TABLE `shf_roles` DISABLE KEYS */;

INSERT INTO `shf_roles` (`id`, `peran`)
VALUES
	(77,'Admin Sub Branch'),
	(88,'Admin Branch'),
	(99,'Super Admin');

/*!40000 ALTER TABLE `shf_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shf_paket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shf_paket`;

CREATE TABLE `shf_paket` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(11) DEFAULT NULL,
  `desc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `shf_paket` WRITE;
/*!40000 ALTER TABLE `shf_paket` DISABLE KEYS */;

INSERT INTO `shf_paket` (`id`, `nama`, `desc`)
VALUES
	(1,'Trial','aktif 3 bualn free'),
	(2,'Paket A','Sewa Per Bulan'),
	(3,'Paket B','Sewa Per 3 Bulan'),
	(4,'Paket C','Sewa per 6 Bulan'),
	(5,'Paket D','Sewa Per 1 Tahun'),
	(6,'Premium','Per 3 tahun'),
	(7,'Ultimate','Sak Lawas e');

/*!40000 ALTER TABLE `shf_paket` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table shf_member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shf_member`;

CREATE TABLE `shf_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(35) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(35) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `role` varchar(2) DEFAULT NULL,
  `status` varchar(2) DEFAULT '1',
  `is_active` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `shf_member` WRITE;
/*!40000 ALTER TABLE `shf_member` DISABLE KEYS */;

INSERT INTO `shf_member` (`id`, `nama`, `username`, `password`, `branch_id`, `role`, `status`, `is_active`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'M Fikri Setiadi','admin','21232f297a57a5a743894a0e4a801fc3',NULL,'88','1',NULL,NULL,NULL,NULL),
	(2,'fikri','kasir','e10adc3949ba59abbe56e057f20f883e',NULL,'77','1',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `shf_member` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
