<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{

    public function __construct(){
		parent::__construct();

    }

    public function cek_login($username, $password){
		$query = $this->db->query(
            "SELECT *
            FROM shf_member
			WHERE 
			email = ".$this->db->escape($username)."
			AND status = 1
			AND deleted_at IS NULL
			; ");

		if ($query->num_rows() > 0) {
			$user = $query->row();
			if(password_verify($password,$user->password)) {
				$data = $user;
			} else {
				$data = FALSE;
			}
		} else {
			$data = FALSE;
		}

		return $data;
	}
	
	public function login($email, $pass){
        $this->db->where('email', $email);
        $this->db->where('password', sha1(sha1(md5($pass))));
        $this->db->where('status', '1');
        $this->db->where('deleted_at IS NULL');
		$log = $this->db->get('shf_member');
		if ($log->num_rows()>0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
    
    public function update_lastlogin($id){
		$this->db->where('id', $id);
		$now = date('Y-m-d H:i:s');
		$data = array(
				'last_login' => $now,
				'updated_at' => $now
			);

		$this->db->update('sfgroup_users', $data);
	}
}