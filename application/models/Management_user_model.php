<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management_user_model extends CI_Model{
    public function __construct(){
		parent::__construct();

    }

    public function get_data(){
        $query = $this->db->query(
            "SELECT *
            FROM sfgroup_users
            WHERE deleted_at IS NULL
            ORDER BY id ASC");
		if ($query->num_rows() > 0) {
			$data = $query->result();
		} else {
			$data = array();
		}

		return $data;
    }
}