<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fungsi_model extends CI_Model{
    public function __construct(){
		parent::__construct();

    }
    
    public function tambah($table, $data){
        $insert = $this->db->insert($table, $data);
		if ($insert){
			return TRUE;
		} else {
			return FALSE;
		}
    }
}