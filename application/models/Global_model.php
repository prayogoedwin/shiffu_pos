<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Global_model extends CI_Model{
    public function __construct(){
		parent::__construct();

    }
    
    public function get_by($where, $where_value, $tabel){
        $this->db->where($where, $where_value);
        return $this->db->get($tabel)->row();
    }

    public function add($tabel, $data){
      return $this->db->insert($tabel, $data);
    }

    public function del($where, $where_value, $tabel){
          $this->db->where($where, $where_value);
          return $this->db->delete($tabel);
    }  

    public function edit($where, $where_value, $tabel, $data){
      $this->db->where($where, $where_value);
      return $this->db->update($tabel, $data);
    }


}