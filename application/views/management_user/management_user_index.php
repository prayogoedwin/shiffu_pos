<!-- Main content -->
<div class="content-wrapper">
    
    <!-- Page header -->
    <div class="page-header border-bottom-0">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            
            <!-- <div class="header-elements d-none mb-3 mb-md-0">
                <div class="d-flex justify-content-center">
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt"></i><span>Statistics</span></a>
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator"></i> <span>Invoices</span></a>
                    <a href="#" class="btn btn-link btn-float text-default"><i class="icon-calendar5"></i> <span>Schedule</span></a>
                </div>
            </div> -->
        </div>
    </div>
    <!-- /page header -->
    
    
    <!-- Content area -->
    <div class="content pt-0">
        <div class="card">

            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>

            <div class="card-body text-right">
                <!-- The <code>DataTables</code> is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table. DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function. Searching, ordering, paging etc goodness will be immediately added to the table, as shown in this example. <strong>Datatables support all available table styling.</strong> -->
                <a class="btn bg-warning-400" id="btn_tambah" data-toggle="modal" data-target="#ModalaAdd">Tambah <i class="icon-add ml-2"></i></a>
            </div>
            <table id="mydata" class="table datatable-basic">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>Nama</th>
                        <th>Last Login</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody id="show_data">
                 
                </tbody>
            </table>
        </div>

        <!-- MODAL ADD START -->
        <div id="ModalaAdd" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah User</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <label class="col-md-4 control-label">Username</label>
                            <div class="col-md-8">
                                <input type="text" class="col-md-12 form-control" id="username" name="username" required>
                                <small>Jangan gunakan tanda spasi</small>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <label class="col-md-4 control-label">Nama</label>
                            <div class="col-md-8">
                                <input type="text" class="col-md-12 form-control" id="nama" name="nama" required>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <label class="col-md-4 control-label">Password</label>
                            <div class="col-md-8">
                                <input type="password" class="col-md-12 form-control" id="pass" name="pass" required>
                            </div>
                        </div>
                        
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="btn_simpan">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- MODAL ADD END -->

    </div>
    <!-- /content area -->

<?php include(__DIR__ . "/../include/footer.php"); ?>

<script>
    $(document).ready(function() {

        tampil_data_managementuser();

        // DataTable
        // $('#mydata').DataTable();
        $('#mydata').DataTable({
            serverSide: true,
            ajax: {
                url: '/data-source',
                type: 'POST'
            }
        });

        //fungsi tampil barang
        function tampil_data_managementuser(){
            $.ajax({
                type  : 'POST',
                url   : '<?=base_url('api/management_user/get');?>',
                dataType : 'json',
                success : function(data){
                    console.log(data.datatable);
                    var result = data.datatable;
                    var html = '';
                    var i;
                    var no = 0;
                    for(i=0; i<result.length; i++){
                        no ++;
                        html += '<tr>'+
                                '<td>'+ no +'</td>'+
                                '<td>'+result[i].username+'</td>'+
                                '<td>'+result[i].nama+'</td>'+
                                '<td>'+result[i].last_login+'</td>'+
                                '<td>'+result[i].aksi+'</td>'+
                                '</tr>';
                    }
                    $('#show_data').html(html);
                }

            });
        }
        

        // $.ajax({
        //     type: "POST",
        //     url: "<?=base_url('api/management_user/get');?>",
        //     success: function(html){
        //         console.log(html.datatable);
        //         var res = html.datatable;
        //         var tabel = $('#dataTable').DataTable({
        //             'destroy'     : true,
        //             'paging'      : true,
        //             'lengthChange': true,
        //             'searching'   : true,
        //             'ordering'    : true,
        //             'info'        : true,
        //             'autoWidth'   : false,
        //             'data'        : res,
        //             'columns': [
        //                 { data: null, sortable : false, searceable : false },
        //                 { data: 'username' },
        //                 { data: 'nama' },
        //                 { data: 'last_login' },
        //                 { data: 'last_login' },
        //                 { data: 'aksi', sortable : false, searceable : false  }
        //             ]
        //         });

        //         tabel.on( 'order.dt search.dt', function () {
        //         tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //             cell.innerHTML = i+1;
        //             } );
        //         } ).draw();
        //     }
        // });


        $('#btn_simpan').on('click',function(){
            var uname = $('#username').val();
            var nm = $('#nama').val();
            var password = $('#pass').val();

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('api/management_user/add')?>",
                dataType : "JSON",
                data : {username:uname , nama:nm, pass:password},
                success: function(data){
                    // console.log(data);
                    var status = JSON.stringify(data.status);
                    var msg = JSON.stringify(data.message);
                    var isTrue = (status === 'true');

                    if(isTrue){
                        $('[name="username"]').val("");
                        $('[name="nama"]').val("");
                        $('[name="pass"]').val("");
                        $('#ModalaAdd').modal('hide');
                        

                        swal(msg, {
                            icon: "success",
                            buttons: {
                                cancel: false,
                                confirm: true,
                            },
                        }).then((btn) => {
                            tampil_data_managementuser();
                        });

                    } else {
                        swal(msg, {
                            icon: "error",
                        });
                    }

                },
                error: function(xhr, ajaxOptions, thrownError){
                    swal("Maaf, terjadi kesalahan " + xhr.responseText, {
                        icon: "error",
                    });
                }
            });
            return false;
        });

    });
</script>
