	<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; <?=date('Y');?>. <a href="#">Shiffu Group</a> by <a href="https://www.instagram.com/nmuhammadsetia/?hl=id" target="_blank"> <span class="text-pink-400"> MSetiaN</span></a>
					</span>

					<!-- <ul class="navbar-nav ml-lg-auto">
						<li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
						<li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a></li>
						<li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>
					</ul> -->
				</div>
			</div>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

	<script src="<?=base_url();?>assets/plugin/sweet/sweetalert.min.js"></script>

	<script>
	$('#btn_logout').on('click', function () {

        swal({
			title: "Apakah anda yakin?",
			text: "Anda akan keluar dari aplikasi",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.ajax({
						type:'POST',
						url:'<?=base_url('api/user/logout'); ?>',
						success:function(data){
							window.location = '<?=base_url();?>';
						},
						error: function(xhr, ajaxOptions, thrownError){
							swal("Maaf, terjadi kesalahan " + xhr.responseText, {
								icon: "error",
							});
						}
					});
				}
			});

    });
	</script>

</body>
</html>