<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shiffu | Always be u're partner</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">

    <style>
    body {
            background: #000 !important;
        }
        .card {
            border: 1px solid #ffff4d;
        }
        .card-login {
            margin-top: 80px;
            padding: 18px;
            max-width: 30rem;
        }

        .card-header {
            color: #fff;
            /*background: #ff0000;*/
            font-family: sans-serif;
            font-size: 20px;
            font-weight: 600 !important;
            margin-top: 10px;
            border-bottom: 0;
        }

        .input-group-prepend span{
            width: 50px;
            background-color: #6699ff;
            color: #fff;
            border:0 !important;
        }

        input:focus{
            outline: 0 0 0 0  !important;
            box-shadow: 0 0 0 0 !important;
        }

        .login_btn{
            width: 130px;
        }

        .login_btn:hover{
            color: #fff;
            background-color: #ff0000;
        }

        .btn-outline-danger {
            color: #fff;
            font-size: 18px;
            background-color: #6699ff;
            background-image: none;
            border-color: #28a745;
        }

        .form-control {
            display: block;
            width: 100%;
            height: calc(2.25rem + 2px);
            padding: 0.375rem 0.75rem;
            font-size: 1.2rem;
            line-height: 1.6;
            color: #28a745;
            background-color: transparent;
            background-clip: padding-box;
            border: 1px solid #ffff4d;
            border-radius: 0;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .input-group-text {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding: 0.375rem 0.75rem;
            margin-bottom: 0;
            font-size: 1.5rem;
            font-weight: 700;
            line-height: 1.6;
            color: #495057;
            text-align: center;
            white-space: nowrap;
            background-color: #e9ecef;
            border: 1px solid #ced4da;
            border-radius: 0;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="card card-login mx-auto text-center bg-dark">
        <div class="card-header mx-auto bg-dark">
            <span> <img src="<?=base_url('assets/images/shiffu.png');?>" class="w-75" alt="Logo"> </span><br/>
                        <span class="logo_title mt-5"> Login Web - admin </span>

        </div>
        <div class="card-body">
            <!-- <form action="" method="post"> -->
                <div class="input-group form-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                    </div>
                    <input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
                </div>

                <div class="input-group form-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                    </div>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                </div>

                <div class="form-group">
                    <a id="btn_login" onclick="doLogin()" class="btn btn-outline-danger float-right login_btn"> Login</a>
                    <!-- <input type="submit" name="btn" value="Login" class="btn btn-outline-danger float-right login_btn"> -->
                </div>

            <!-- </form> -->
        </div>
    </div>
</div>

<script src="<?=base_url();?>assets/plugin/sweet/sweetalert.min.js"></script>
<script src="<?=base_url();?>assets/plugin/js/jquery-3.4.1.js"></script>

<script>
    // $('#btn_login').on('click', function () {

    //     var user = $('#username').val();
    //     var pass = $('#password').val();

    //     $("#password").keyup(function(event) {
    //         if (event.keyCode === 13) {
    //             $("#btn_login").click();
    //         }
    //     });
    // });
</script>

<script>
    $('#btn_login').on('click', function () {
        var user = $('#username').val();
        var pass = $('#password').val();

        if(user != '' && pass != ''){
            swal("Mohon tunggu", {
                icon: "info",
                buttons: false,
                closeModal: false,
                closeOnClickOutside: false,
                closeOnEsc: false,
                icon: '<?=base_url("assets/images/index.flip-circle-google-loader-gif.svg");?>'
            });

            $.ajax({
                type: "POST",
                url: "<?=base_url('api/user/login');?>",
                data: { 
                    'username': user, 
                    'password': pass
                },
                success: function(response){
                    console.log(response.id);
                    var status = JSON.stringify(response.status);
                    var msg = JSON.stringify(response.message);
                    var isTrue = (status === 'true');

                    if(isTrue){

                        $.get("api/user/set_session/" + response.id, function (res) {
                            console.log(res);
                        })

                        swal("Login berhasil ...", {
                            icon: "success",
                            buttons: {
                                cancel: false,
                                confirm: true,
                            },
                        }).then((btn) => {
                            
                            window.location = '<?=base_url('dashboard');?>';
                        });
                        
                    } else {
                        swal(msg, {
                            icon: "error",
                        });
                    }
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.responseText);
                    swal("Maaf, terjadi kesalahan " + xhr.responseText, {
                        icon: "error",
                    });
                }
            });
        } else {
            swal("Username dan password tidak boleh kosong", {
                icon: "info",
            });
        }
    });
</script>

</body>
</html>