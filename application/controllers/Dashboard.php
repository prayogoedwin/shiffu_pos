<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct(){
        parent::__construct();

        $lgn = $this->session->userdata();

        if(!isset($lgn['logged_in'])) {
			redirect('/', 'refresh');
		}
    }

    public function index(){
        // $userinfo = $this->session->all_userdata();

        // echo json_encode($userinfo);

        $this->load->view('include/header');
        $this->load->view('include/sidebar');
        $this->load->view('dashboard');
    }
}