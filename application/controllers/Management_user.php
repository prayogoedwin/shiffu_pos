<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management_user extends CI_Controller {

    public function __construct(){
        parent::__construct();

        $lgn = $this->session->userdata();

        if(!isset($lgn['logged_in'])) {
			redirect('/', 'refresh');
        }
        
        $this->load->model('Management_user_model');
    }

    public function index(){

        $this->load->view('include/header');
        $this->load->view('include/sidebar');
        $this->load->view('management_user/management_user_index');
    }
}