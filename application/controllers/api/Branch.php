<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends CI_Controller {

    public function __construct(){
		parent::__construct();
    }

    public function edit(){
        $branch_id  = $this->input->post('branch_id', TRUE);
        $nama       = $this->input->post('nama', TRUE);
        $alamat     = $this->input->post('alamat', TRUE);
        $telp       = $this->input->post('telp', TRUE);
        $hp         = $this->input->post('hp', TRUE);
        $datenow = date('Y-m-d H:i:s');
        
        $data = array(
            'nama'  => $nama,
            'alamat'=> $alamat,
            'telp'  => $telp,
            'hp'    => $hp,
            'created_at' => $datenow

        );
        
        $update = $this->Global_model->edit('id', $branch_id, 'shf_branch', $data);
        if($update){
            $get_branch = $this->Global_model->get_by('id', $branch_id, 'shf_branch');
            $respon = array(
                'status'  => true,
                'message' => 'Berhasil Update',
                'data'    => $get_branch,
            );
            header('Content-Type: application/json');
            echo json_encode($respon);  
        }else{
            $respon = array(
                'status'  => false,
                'message' => 'gagal update',
                'data'    => $array,
            );
            header('Content-Type: application/json');
            echo json_encode($respon);  
        }


    }   

}