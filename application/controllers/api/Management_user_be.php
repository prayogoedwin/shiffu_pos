<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management_user_be extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$this->load->model('Management_user_model');
		$this->load->model('Fungsi_model');
    }

    public function get(){
        // $token = $this->input->post('token');
        $get = $this->Management_user_model->get_data();

		$datatable = array();
		foreach ($get as $key => $value) {
		    $action = 
				'
				<a class="btn btn-outline bg-teal-400 btn-icon rounded-round" href="'. base_url('manajemen_user/ganti_password/') .  $value->id .'"><i class="icon-key"></i></a>
				<a class="btn btn-outline bg-indigo-400 btn-icon rounded-round" href="'. base_url('manajemen_user/edit/') .  $value->id .'"><i class="icon-compose"></i></a>
				<a class="btn btn-outline bg-pink-400 btn-icon rounded-round" href="'. base_url('manajemen_user/hapus/') .  $value->id .'"><i class="icon-trash"></i></a>
				';

			$last_login = '-';
			if ($value->last_login != null) {
				$last_login = date('d-m-Y H:i:s', strtotime($value->last_login));
			}

			$datatable[$key] = array(
                'username' => $value->username,
                'nama' => $value->name,
				'last_login' => $last_login,
				'aksi' => $action
			);
		}
        $data['datatable'] = $datatable;
        
        header('Content-Type: application/json');
        echo json_encode($data);
        
        // echo $token;
	}
	
	public function tambah_action(){
		$username = $this->input->post('username');
        $nama = $this->input->post('nama');
		$pass = $this->input->post('pass');
		$data = array(
			'username' => $username,
			'name' => $nama,
			'password' => $pass
		);

		$insert = $this->Fungsi_model->tambah('sfgroup_users', $data);
		if ($insert) {

			$respon = array(
                'status' => true,
                'message' => 'Berhasil menambahkan data'
			);
			
		} else {

			$respon = array(
                'status' => false,
                'message' => 'Gagal menambahkan data'
			);
			
		}

		header('Content-Type: application/json');
        echo json_encode($respon);
	}
}