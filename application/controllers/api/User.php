<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model('User_model');
    }

    public function register(){
        $nama       = $this->input->post('nama', TRUE);
        $email      = $this->input->post('email', TRUE);
        $password   = $this->input->post('password', TRUE);
        $password2  = $this->input->post('password2', TRUE);
        $datenow = date('Y-m-d H:i:s');
        if($password != $password2){
            $respon = array(
                'status'  => false,
                'message' => 'Password Yang Anda Masukkan Tidak Sama',
                'data'    => NULL,
            );
            header('Content-Type: application/json');
            echo json_encode($respon);
        }else{
            $cek_email = $this->db->query("SELECT COUNT(email) as hasil FROM shf_member WHERE email = '$email'")->row();
            if($cek_email->hasil > 0){
                $respon = array(
                    'status'  => false,
                    'message' => 'Email Sudah Terdaftar',
                    'data'    => NULL,
                );
                header('Content-Type: application/json');
                echo json_encode($respon);
            }else{
                  //email
                  $encrypted_id = sha1($this->input->post('email',TRUE));
                  $this->load->library('email');
                  $config = array();
                  $config['charset'] = 'utf-8';
                  $config['useragent'] = 'Codeigniter';
                  $config['protocol']= "smtp";
                  $config['mailtype']= "html";
                  $config['smtp_host']= "smtp.gmail.com";
                  $config['smtp_port']= "465";
                  $config['smtp_timeout']= "400";
                  $config['smtp_user']= "validasipendaftaran@gmail.com"; 
                  $config['smtp_pass']= "7ateng6ayen9"; 
                  $config['crlf']="\r\n";
                  $config['newline']="\r\n";
                  $config['wordwrap'] = TRUE;
                  $this->email->initialize($config);
                  //konfigurasi pengiriman
                  $this->email->from($config['smtp_user']);
                  $this->email->to($email);
                  $this->email->subject("Verifikasi Akun");
                  $this->email->message(
                  "Terimakasih telah melakuan registrasi Shiffu POS, untuk mengaktifkan akun Anda silahkan klik tombol dibawah ini :<br>
                    <a href='".site_url("publik/verifikasi/$encrypted_id")."'><button style='background-color: cyan;border: 1px solid #b1b1b1; ox-shadow: 2px 2px 8px rgba(0, 0, 0, 0.5);border-radius:3px; font-size: 18px; font-family: sans-serif; box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.4);'>AKTIVASI AKUN</button><a/>"
                  );
                    if($this->email->send()){
                            // $options = [
                            //     'cost' => 11
                            // ];
                            // $hash_password = password_hash($password, PASSWORD_BCRYPT, $options);
                            $data = array(
                                'nama'      => $nama,
                                'email'     => $email,
                                'password'  => sha1(sha1(md5($password))),
                                'branch_id' => md5($email),
                                'role'      => 88,
                                'created_at'=> $datenow,
                                'token'     => $encrypted_id
                            );
                            $add       = $this->Global_model->add('shf_member', $data);
                            $insert_id = $this->db->insert_id();
                            $get       = $this->Global_model->get_by('id', $insert_id, 'shf_member');
                            if($add){
                                $data_branch = array(
                                    'id' => $get->branch_id,
                                    'created_at' => $datenow
                                );
                                $add_branch       = $this->Global_model->add('shf_branch', $data_branch);
                                if($add_branch){
                                    $respon = array(
                                        'status'  => true,
                                        'message' => 'Selamat Anda Berhasil Mendaftar Shiffu POS, Silakan buka email untuk aktivasi akun',
                                        'data'    => NULL,
                                    );
                                    header('Content-Type: application/json');
                                    echo json_encode($respon);   
                                }else{
                                    $gagal_then_hapus = $this->Global_model->del('id', $get->id, 'shf_member');
                                    if($gagal_then_hapus){
                                        $respon = array(
                                            'status'  => true,
                                            'message' => '1. Pendaftaran Gagal, silakan ulangi kembali',
                                            'data'    => NULL,
                                        );
                                        header('Content-Type: application/json');
                                        echo json_encode($respon);   
                                    }
                                } 
                            }else{
                                $respon = array(
                                    'status'  => false,
                                    'message' => '2. Pendaftaran Gagal, Silakan ulangi kembali',
                                    'data'    => NULL,
                                 );
                                 header('Content-Type: application/json');
                                 echo json_encode($respon);  
                            }
                    }else{
                        $respon = array(
                            'status'  => false,
                            'message' => 'Email Tidak Terkirim, Mungkin Anda salah memasukkan email',
                            'data'    => NULL,
                        );
                        header('Content-Type: application/json');
                        echo json_encode($respon);  
                    }
                
            }
        }
    }

    public function validasi_email($token){
        $get   = $this->db->query("SELECT count(token) as tkn FROM shf_member WHERE token = '$token'")->row();
        if($get->tkn == 1){
            echo "Selamat Akun Anda sudah terverifikasi";
        }else{
            echo "Gagal verifikasi";
        }
    }
    
    public function login(){
        $username = $this->input->post('email', TRUE);
        $password = $this->input->post('password', TRUE);
        $cek = $this->User_model->login($username, $password);
        if($cek){
            $get = $this->Global_model->get_by('email', $username, 'shf_member');
            $get_branch = $this->Global_model->get_by('id', $get->branch_id, 'shf_branch');
            $data = array(
                'akun' => $get,
                'branch' => $get_branch
            );
            if($get_branch->nama == NULL){
                $respon = array(
                    'status'  => true,
                    'message' => 'TIDAK LENGKAP, BERHASIL LOGIN',
                    'data'    => $data,
                   
                );
                header('Content-Type: application/json');
                echo json_encode($respon);  
            }else{
                
                $respon = array(
                    'status'  => true,
                    'message' => 'LENGKAP, BERHASIL LOGIN',
                    'data'    => $data,
                   
                );
                header('Content-Type: application/json');
                echo json_encode($respon);  
            }
        } else {
            $respon = array(
                'status'  => false,
                'message' => 'Gagal, Periksa User dan Password',
                'data'    => array(),
            );
            header('Content-Type: application/json');
            echo json_encode($respon);
        }
    }

    public function logout(){
        $array_items = array('id', 'logged_in');
        $this->session->unset_userdata($array_items);
    }
    
}